import { Injectable } from '@angular/core';
import { Recipe } from './recipe.class';
import { Ingredient } from './ingredient.class';
import { Fridge } from './fridge.class';
import { ShoppingList } from './shoppinglist.class';
import { HttpRequest , HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class recipemanagementhttpService {

    newrecipe: Array<Recipe>;
    public ing1 : Ingredient = new Ingredient("apple", 5);
    public ing2 : Ingredient = new Ingredient("mango",6);
	public ing3 : Ingredient = new Ingredient("bananas",12);

	public fridge1 : Fridge = new Fridge();
  public recipeList = new Array<Recipe>();
  public selectedRecipe = new Recipe();

  public ingredientsNeeded : Array<Ingredient> = new Array<Ingredient>();


  constructor(private http : HttpClient) {
    

    this.recipeList.push(new Recipe());
    this.recipeList[0].addItem(this.ing1);
    this.recipeList[0].addItem(this.ing2);
    this.recipeList[0].addItem(this.ing3);
    
    this.fridge1.add("apple", 2 );
		this.fridge1.add("mango", 7);
    
   }


   generateShoppigList(){
    this.ingredientsNeeded = this.fridge1.checkrecipe(this.selectedRecipe)[1];
    console.log("list is:" + this.ingredientsNeeded.length);
  }

  addrecipe( recipe : Recipe){
    console.log(recipe);
    this.http.post("http://localhost:8000/addrecipe", recipe)
    .toPromise()
    .catch((reason)=>{
      console.log("order failed",reason);
    }).then((response : any)=>{
      console.log("response",response);

      let newRecipes = response.recipeList.map((value)=>{
         let recipe = new Recipe();
         recipe.ingredientList = value;
        
         return recipe;
      })
      this.recipeList = newRecipes;
    });
  }


  retrieverecipe() : Promise<any>{
  
   let request = this.http
    .request(new HttpRequest("GET","http://localhost:8000/retrieverecipe/:name"))
    .toPromise().catch((reason)=>{
      console.log(reason);
      return reason;
    });

    
    request.then((response: any)=>{
      console.log(response.body);
      return response;
    });
    return request;     

  }


  ngOnInit() {
    console.log("list is:" + this.ingredientsNeeded.length);
  }

  init(){
    console.log("I am in the http service");
     this.retrieverecipe().then((response: any)=>{
      this.newrecipe = response.body.newrecipe;
      this.newrecipe = response.body.newrecipe;
      return response;
     }).catch(()=>{})

 }

}
