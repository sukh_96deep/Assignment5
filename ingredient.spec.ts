/*
    Sukhmandeep Singh Bassi
    100275305
    Assingment#1 - CPSC 2261
**/
import {Ingredient} from "./ingredient.class"

describe("ingredient testing ", function(){

    let fruit = "orange";
    let qty = 5;
    let orange = new Ingredient(fruit, qty); 
    
    // checking if the name is same as given/entered by method
    it("has name", function(){
        expect(orange.name).toBe(fruit);
    });

    // chechking if the qty is same as given/entered by method
    it("has quantity", function(){
        expect(orange.quantity).toBe(qty);
    });

    // checking if the qty is added
    it("is added", function(){
        let addedAmt: number = 10;
        orange.add(addedAmt);
        expect(orange.quantity).toBe(addedAmt+qty);
        console.log(addedAmt+qty); // just for printing 
    });

    // checking if qty is subracted 
    it("is subtracted", function(){
        expect(orange.quantity).toBe(15);
        let subAmt: number = 20;
        let temp = orange.quantity;
        expect(temp).toBe(15);
        orange.subtract(subAmt);
        expect(orange.quantity).toBe( temp - subAmt);
      //  console.log(orange.quantity);
       // console.log(temp - subAmt);
       
    });
});
