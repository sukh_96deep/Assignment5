/*
    Sukhmandeep Singh Bassi
    100275305
    Assingment#1 - CPSC 2261
**/
import {Ingredient} from "./ingredient.class"
import {Recipe} from "./recipe.class"

export class Fridge{
    contents: Array<Ingredient> = new Array <Ingredient>();
    
    constructor(){
    }

    add(name: string, qty: number){
       
        for(var i=0; i<this.contents.length; i++){
            if (this.contents[i].name == name){
                this.contents[i].add(qty);                
                    return;
            }
        }
        
        this.contents.push(new Ingredient(name, qty));
        return;
    }   

    remove(name: string, qty: number){
        for(var i=0; i<this.contents.length; i++){
            if (this.contents[i].name == name){
                this.contents[i].subtract(qty);
                if(this.contents[i].quantity <= 0){
                    this.contents.splice(i,1);
                    return;
                }
            }
        }
    }

    checkrecipe(recipeA : Recipe){

        let lists : Ingredient[][]= [[],[]];
        
        for(let i of this.contents){
            for(let j of recipeA.ingredientList){
                let ab = Math.abs(i.quantity - j.quantity);
                if (i.name === j.name && ab<= 2 && ab>0){
                    for(let m of lists){
                        m.push(j);
                    }
                }else if(i.name === j.name && i.quantity < j.quantity){
                    lists[0].push(j);
                }else if(i.name=== j.name && i.quantity > j.quantity){
                    lists[1].push(j);
                }
            }
        }

       
        for(let j of recipeA.ingredientList){
            if(!lists[0].includes(j) && !lists[1].includes(j)){
                lists[0].push(j);
            }
        }

        for(let i of lists){
            if(i.length === 0){
                i.push(null);
            }
        }
        return lists;
       
    }
}
