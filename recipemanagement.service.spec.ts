import { TestBed, inject } from '@angular/core/testing';

import { RecipemanagementService } from './recipemanagement.service';

describe('RecipemanagementService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RecipemanagementService]
    });
  });

  it('should be created', inject([RecipemanagementService], (service: RecipemanagementService) => {
    expect(service).toBeTruthy();
  }));
});
