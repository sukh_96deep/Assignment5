import { Injectable } from '@angular/core';
import { Recipe } from './recipe.class';
import { Ingredient } from './ingredient.class';
import { Fridge } from './fridge.class';
import { ShoppingList } from './shoppinglist.class';
import { HttpHeaders , HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class RecipemanagementService {

  public ing1 : Ingredient = new Ingredient("apple", 5);
	public ing2 : Ingredient = new Ingredient("mango",6);
	public ing3 : Ingredient = new Ingredient("bananas",12);

	public fridge1 : Fridge = new Fridge();
  public recipeList = new Array<Recipe>();
  public selectedRecipe = new Recipe();

  public ingredientsNeeded : Array<Ingredient> = new Array<Ingredient>();


  constructor() {
    

    this.recipeList.push(new Recipe());
    this.recipeList[0].addItem(this.ing1);
    this.recipeList[0].addItem(this.ing2);
    this.recipeList[0].addItem(this.ing3);
    
    this.fridge1.add("apple", 2 );
		this.fridge1.add("mango", 7);
    
   }


   generateShoppigList(){
    this.ingredientsNeeded = this.fridge1.checkrecipe(this.selectedRecipe)[1];
    console.log("list is:" + this.ingredientsNeeded.length);
  }

  
  ngOnInit() {
    console.log("list is:" + this.ingredientsNeeded.length);
  }

  init(){
    console.log("I am not in the regular service");
 }

}
