/*
    Sukhmandeep Singh Bassi
    100275305
    Assingment#1 - CPSC 2261
**/
import {Ingredient} from "./ingredient.class"
export class Recipe{

    time: number;
    ingredientList: Array<Ingredient> = new Array<Ingredient>();
    instructionList: Array<String> = new Array<String>();
    
    // constructor(time:  number, ingredients: Array<Ingredient>, instructions: Array<String>){
    //     this.time = time;
    //     this.ingredientList = ingredients.slice();
    //     this.instructionList = instructions.slice();
    // }

    //adding the item 
    addItem(item: Ingredient) {
        this.ingredientList.push(item);  
    }
    // adding the instrcution
    addinstrcuction(instrc: String){
        this.instructionList.push(instrc);
    }

    addTime(time: number){
        this.time = time;
    }
 
}
