
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RecipemanagementService } from './recipemanagement.service';
import { recipemanagementhttpService} from './recipemanagementhttp.service'
import { AppComponent } from './app.component';
import { RecipeManagementComponent } from './recipe-management/recipe-management.component';
import { FridgeComponent } from './fridge/fridge.component';
import { ShoppinglistComponent } from './shoppinglist/shoppinglist.component';
import { HttpClientModule } from '@angular/common/http';
//import {RouterModule} from '@angular/router'

@NgModule({
  declarations: [
    AppComponent,
    RecipeManagementComponent,
    FridgeComponent,
    ShoppinglistComponent, 
    // RouterModule.forRoot([
    //   {path: '', component: AppComponent},
    //   {path: '**', component: NotFoundComponent}
    // ])
  ],
  imports: [
    BrowserModule,
    HttpClientModule, 
    FormsModule
  ],
  providers: [{ provide: RecipemanagementService, useClass: recipemanagementhttpService }],
  bootstrap: [AppComponent], 
  
})
export class AppModule { }
